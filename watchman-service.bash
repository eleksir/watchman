#!@BASH_PATH@

watchman_version='@VERSION@'

# Check if XDG_ stuff is set
XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR:-"/run/user/$UID"}
XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-"$HOME/.config"}

[[ "$1" == '-v' ]] && {
	printf '%s\n' "$watchman_version"
	exit 0
}

cfg_init_dirs=( @INITDIRS@ )

service="$1"; shift

for i in "${cfg_init_dirs[@]}"; do
	[[ -f "${i}/${service}" ]] && {
		service_script="${i}/${service}"
	}
done

[[ "$service_script" ]] || {
	printf '%s\n' "Service $service not found."
	exit 1
}

"$service_script" "$@"
