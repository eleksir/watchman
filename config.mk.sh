# build.sh config

VERSION=v1.11.2

PREFIX=${PREFIX:-/opt}
USR=${USR:-"${PREFIX}usr"}

BINDIR=${BINDIR:-"${DESTDIR}${USR}/bin"}

CFGDIR=${CFGDIR:-"${PREFIX}etc/watchman"}
LOGDIR=${LOGDIR:-"/share/MD0_DATA/Public/logs/watchman"}
RUNDIR=${RUNDIR:-"${PREFIX}run/watchman"}

CFGDIR_USER=${CFGDIR_USER:-"\$XDG_CONFIG_HOME/watchman"}
LOGDIR_USER=${LOGDIR_USER:-"\$HOME/.local/var/log/watchman"}
RUNDIR_USER=${RUNDIR_USER:-"\$XDG_RUNTIME_DIR/watchman/run"}

INITDIRS="\"${CFGDIR}/init.d\" \"${CFGDIR_USER}/init.d\""

TOOLS=${TOOLS-"runas service svc"}

BASH_PATH=${BASH_PATH:-"/bin/bash"}
