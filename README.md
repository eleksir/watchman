watchman
========

Yet another service manager.

Installation
------------
### Arch Linux:
#### Latest PKGBUILDS:

 * https://aur4.archlinux.org/packages/watchman-sm
 * https://pkg.fleshless.org/watchman-sm/tree

#### Manual:

##### Linux:
	git clone https://github.com/fbt/watchman.git
	cd watchman
	./make.sh
	./make.sh install

##### FreeBSD:
FreeBSD doesn't have /run as a tmpfs (or anything), so you would need to tell watchman where to keep runtime files

	git clone https://github.com/fbt/watchman.git
	cd watchman
	RUNDIR=/var/run/watchman ./make.sh
	./make.sh install

##### Others:
I'e not personally tested watchman on any other systems. Someone told me it works on Solaris and OpenBSD. Test it and get in touch with me if you want.

Services
--------
A service is a script in the watchman's init.d directory.  
By default it's /etc/watchman/init.d for system services and $HOME/.watchman/init.d for user ones.
Watchman creates relevant dirs if they don't exist.

Variables
---------
There are some variables you can use to alter a service's behaviour.  
All of them are optional. Yes, I mean all of them.

	$service_command	# The service's executable. Watchman will try to find the service in $PATH by the service name, so yes,
							even this is optional.
	$service_args		# Arguments to $service_command
	$service_respawn	# Set this to restart the service if it exists. Only works with services that don't daemonize.
	$service_name		# It is useful to set $service_name explicitly if you use a symlink to refer to a service and want to use 
							both the original script and the symlink.
	$service_workdir	# The directory to cd to before running $service_command
	$service_pidfile	# The service's pid file. Will not be overwritten by watchman. By specifying this, you basically tell
							watchman that the service mantains its pidfile itself.
	$service_logfile	# The file with $service_command's stdout/stderr.
	$service_actions	# Array. Available service actions. Should not be overriden, only added to. If you need to remove
							an action, just do unset $action.

Functions
---------

Standart functions:

	start()     # Start the service. Calls watchman.start().
	stop()      # Stop the service. Calls watchman.stop().
	restart()   # Restart the service. Calls stop(), then start().
	reload()    # Reload the service's configuration. Calls watchman.reload, which sends SIGHUP to the service.
	status()    # Show the service's status. Calls watchman.status().
	depends()   # Starts the specified services. Calls watchman.depends.
	logs()      # Shows the service stdout log. Calls watchman.logs.
