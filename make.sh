#!/bin/sh
set -e

# Functions
set_vars() {
	printf 'Setting vars...\n' >&2

	sed -r \
		-e "s%@VERSION@%${VERSION}%" \
		-e "s%@CFGDIR_USER@%${CFGDIR_USER}%" \
		-e "s%@RUNDIR_USER@%${RUNDIR_USER}%" \
		-e "s%@LOGDIR_USER@%${LOGDIR_USER}%" \
		-e "s%@CFGDIR@%${CFGDIR}%" \
		-e "s%@RUNDIR@%${RUNDIR}%" \
		-e "s%@LOGDIR@%${LOGDIR}%" \
		-e "s%@INITDIRS@%${INITDIRS}%" \
		-e "s%@BASH_PATH@%${BASH_PATH}%"
}

do_tell() {
	printf 'Executing: %s\n' "$*"
	"$@"
}

# Targets
target_watchman() {
	target='watchman'

	printf '%s\n' "Building watchman..."
	set_vars < "$target".bash > "$target"
	do_tell chmod 755 "$target"
	ls -l "$target"
}

target_tools() {
	if [ "$TOOLS" ]; then
		do_tell mkdir -p tools

		for i in $TOOLS; do
			printf '%s\n' "Building ${i}..."
			set_vars < "watchman-${i}.bash" > "tools/watchman-$i"
			do_tell chmod 755 "tools/watchman-$i"
		done

		ls -l tools/*
	fi
}

target_clean() {
	do_tell rm -f tools/* watchman build
	if [ -d tools ]; then
		do_tell rmdir tools
	fi
}

target_install() {
	mkdir -p "$BINDIR"

	do_tell cp watchman "${BINDIR}/watchman"
	do_tell chmod 755 "${BINDIR}/watchman"

	for i in $TOOLS; do
		do_tell cp "tools/watchman-$i" "$BINDIR/watchman-$i"
		do_tell chmod 755 "$BINDIR/watchman-$i"
	done
}

target_all() {
	target_watchman
	target_tools
}

gitver() {
	if last_tag=$( git describe --long --tags 2>/dev/null ); then
		printf '%s\n' "$last_tag" | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
	else
		printf "git-r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
	fi
}

main() {
	while [ -n "$1" ]; do
		case "$1" in
			*=*) export "${1%%=*}=${1#*=}";;
			--) shift; break;;
			*) break;;
		esac
		shift
	done

	target="${1:-all}"

	# Build configuration
	. './config.mk.sh'

	printf '%s\n' "Building target: $target"

	if [ -d .git ]; then
		VERSION=$(gitver)
		printf '%s\n' "Building from git, overriding version: $VERSION"
	fi

	"target_$target"
}

main "$@"
